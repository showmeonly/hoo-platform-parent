-- release_simple 版本 MySQL 脚本
/*
 Navicat Premium Data Transfer

 Source Server         : permission_platform
 Source Server Type    : MySQL
 Source Server Version : 50639
 Source Host           : 127.0.0.1:3306
 Source Schema         : permission_platform2

 Target Server Type    : MySQL
 Target Server Version : 50639
 File Encoding         : 65001

 Date: 24/11/2020 20:00:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_sys_app
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_app`;
CREATE TABLE `t_sys_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
  `app_key` varchar(10) DEFAULT NULL COMMENT 'app key （唯一,分配不可更改）',
  `secret` varchar(64) DEFAULT NULL COMMENT '系统接入密钥',
  `name` varchar(255) DEFAULT NULL COMMENT '系统接入名称(applicaiton.name)',
  `description` varchar(255) DEFAULT NULL COMMENT '系统描述',
  `frontend` varchar(25) DEFAULT NULL COMMENT '前端类型（VUE、ExtJS、CUSTOM、React、Angular）',
  `sort` int(11) DEFAULT NULL COMMENT '排序序号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `app_key` (`app_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='暂未使用 - 系统接入应用表，用于支持SSO多平台权限控制';

-- ----------------------------
-- Records of t_sys_app
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_app` VALUES (2, 'ROir46vI', 'c7b1825abb0a82879575d1b5affa924e618efe74', '权限系统-默认', '默认对接系统', 'vue', 0);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_resource`;
CREATE TABLE `t_sys_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
  `parent_id` int(11) DEFAULT NULL COMMENT '父ID',
  `name` varchar(255) DEFAULT NULL COMMENT '资源名称（菜单名称、按钮名称）',
  `icon` varchar(25) DEFAULT NULL COMMENT '图标',
  `url` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `path` varchar(255) DEFAULT NULL COMMENT '组件路径(如VUE场景)',
  `hide` int(1) DEFAULT NULL COMMENT '0 不隐藏， 1 隐藏',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `sort` int(11) DEFAULT NULL COMMENT '排序序号',
  `type` int(1) DEFAULT '1' COMMENT '资源类型 1: 菜单，2：按钮',
  `status` varchar(1) DEFAULT '1' COMMENT '资源状态 1：可用，0：禁用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COMMENT='系统-资源表(菜单、按钮)';

-- ----------------------------
-- Records of t_sys_resource
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_resource` VALUES (77, 0, '权限管理', 'setting', '/permission', 'BlankLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (78, 77, '用户管理', 'user', '/permission/user', 'permission/UserList', 0, 'sys:user:list', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (79, 77, '角色管理', 'usergroup-add', '/permission/role', 'permission/RoleList', 0, 'sys:role:list', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (80, 77, '资源管理', 'bars', '/permission/resource', 'permission/ResourceList', 0, 'sys:resource:list', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (81, 78, '用户-新增', NULL, NULL, NULL, 0, 'sys:user:save', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (82, 80, '资源-新增', NULL, NULL, NULL, 0, 'sys:resource:save', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (83, 80, '资源-修改', NULL, NULL, NULL, 0, 'sys:resource:update', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (84, 80, '资源-删除', NULL, NULL, NULL, 0, 'sys:resource:delete', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (85, 78, '用户-修改', NULL, NULL, NULL, 0, 'sys:user:update', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (86, 78, '用户-删除', NULL, NULL, NULL, 0, 'sys:user:delete', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (87, 79, '角色-新增', '', '', '', 0, 'sys:role:save', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (88, 79, '角色-修改', '', '', '', 0, 'sys:role:update', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (89, 79, '角色-删除', '', '', '', 0, 'sys:role:delete', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (90, 77, '数据字典', 'book', '/permission/dictionary', 'permission/DictionaryList', 0, 'sys:dictionary:list', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (91, 0, '外部链接', 'branches', 'https://www.baidu.com/', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (92, 0, 'iframe', 'qq', 'http://www.qq.com', 'AIframe', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (93, 78, ' 用户-启用', '', '', '', 0, 'sys:user:useable', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (94, 78, ' 用户-禁用', '', '', '', 0, 'sys:user:disabled', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (95, 78, ' 用户-锁定', '', '', '', 0, 'sys:user:locked', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (96, 78, ' 用户-解锁', '', '', '', 0, 'sys:user:unlock', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (97, 79, '角色-禁用', NULL, NULL, NULL, 0, 'sys:role:disabled', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (98, 79, '角色-启用', NULL, NULL, NULL, 0, 'sys:role:useable', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (99, 80, '资源-禁用', NULL, NULL, NULL, 0, 'sys:resource:disabled', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (100, 80, '资源-启用', NULL, NULL, NULL, 0, 'sys:resource:useable', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (101, 78, '用户-导出', NULL, NULL, NULL, 0, 'sys:user:export', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (102, 79, '角色-导出', NULL, NULL, NULL, 0, 'sys:role:export', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (103, 80, '资源-导出', NULL, NULL, NULL, 0, 'sys:resource:export', 0, 2, '1');
INSERT INTO `t_sys_resource` VALUES (104, 0, '统一资源', 'global', 'http://localhost:8000', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (105, 0, '代码生成', 'code', 'http://localhost:8000', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (106, 0, '数据可视化', 'fund', 'http://localhost:8000', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (107, 0, 'SimpleQuery', 'thunderbolt', 'http://localhost:8000', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (108, 0, '元数据管理', 'table', 'http://localhost:8000', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (109, 0, 'Swagger3文档', NULL, 'http://localhost:13001/swagger-ui/index.html#/', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (110, 0, 'Druid监控', NULL, 'http://localhost:13001/druid/index.html', 'EmptyLayout', 0, '', 0, 1, '1');
INSERT INTO `t_sys_resource` VALUES (111, 0, 'Iframe测试', NULL, 'https://www.w3cschool.cn/tutorial', 'AIframe', 0, '', 0, 1, '1');
COMMIT;

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
  `role_key` varchar(32) DEFAULT NULL COMMENT '唯一识别标记（不可修改）',
  `name` varchar(255) DEFAULT NULL COMMENT '角色标记',
  `description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `status` varchar(1) DEFAULT '1' COMMENT '角色-状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_key` (`role_key`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COMMENT='系统-角色表';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_role` VALUES (7, 'SUPER_ADMIN', '超级管理员', '系统最高用户,拥有所有权限', '1');
INSERT INTO `t_sys_role` VALUES (8, 'TEST_KEY', 'TEST ROLE', '测试变更', '1');
INSERT INTO `t_sys_role` VALUES (9, 'ADMIN', '管理员', '系统管理员', '1');
INSERT INTO `t_sys_role` VALUES (10, 'READONLY', '只读用户', '所有资源只读', '1');
INSERT INTO `t_sys_role` VALUES (22, 'SSO_USER', '统一登录用户角色', '维护统一登录这类菜单角色（项目待拆分）', '1');
INSERT INTO `t_sys_role` VALUES (23, NULL, '超级管理员-update', '系统最高用户,拥有所有权限', NULL);
INSERT INTO `t_sys_role` VALUES (24, NULL, '超级管理员-update', '系统最高用户,拥有所有权限', NULL);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_role_app_____del
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_app_____del`;
CREATE TABLE `t_sys_role_app_____del` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `app_id` int(11) NOT NULL COMMENT '应用ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='考虑删除，完全可以通过 菜单链接控制 ，即 统一桌面的菜单形式';

-- ----------------------------
-- Table structure for t_sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_resource`;
CREATE TABLE `t_sys_role_resource` (
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
  `resource_id` int(11) DEFAULT NULL COMMENT '资源ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-角色资源关系表';

-- ----------------------------
-- Records of t_sys_role_resource
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_role_resource` VALUES (10, 77);
INSERT INTO `t_sys_role_resource` VALUES (10, 78);
INSERT INTO `t_sys_role_resource` VALUES (10, 79);
INSERT INTO `t_sys_role_resource` VALUES (10, 80);
INSERT INTO `t_sys_role_resource` VALUES (9, 77);
INSERT INTO `t_sys_role_resource` VALUES (9, 78);
INSERT INTO `t_sys_role_resource` VALUES (9, 79);
INSERT INTO `t_sys_role_resource` VALUES (9, 80);
INSERT INTO `t_sys_role_resource` VALUES (9, 81);
INSERT INTO `t_sys_role_resource` VALUES (9, 85);
INSERT INTO `t_sys_role_resource` VALUES (9, 91);
INSERT INTO `t_sys_role_resource` VALUES (9, 92);
INSERT INTO `t_sys_role_resource` VALUES (9, 93);
INSERT INTO `t_sys_role_resource` VALUES (9, 94);
INSERT INTO `t_sys_role_resource` VALUES (9, 95);
INSERT INTO `t_sys_role_resource` VALUES (9, 96);
INSERT INTO `t_sys_role_resource` VALUES (9, 101);
INSERT INTO `t_sys_role_resource` VALUES (22, 104);
INSERT INTO `t_sys_role_resource` VALUES (22, 105);
INSERT INTO `t_sys_role_resource` VALUES (22, 106);
INSERT INTO `t_sys_role_resource` VALUES (22, 107);
INSERT INTO `t_sys_role_resource` VALUES (22, 108);
INSERT INTO `t_sys_role_resource` VALUES (7, 77);
INSERT INTO `t_sys_role_resource` VALUES (7, 78);
INSERT INTO `t_sys_role_resource` VALUES (7, 79);
INSERT INTO `t_sys_role_resource` VALUES (7, 80);
INSERT INTO `t_sys_role_resource` VALUES (7, 81);
INSERT INTO `t_sys_role_resource` VALUES (7, 82);
INSERT INTO `t_sys_role_resource` VALUES (7, 83);
INSERT INTO `t_sys_role_resource` VALUES (7, 84);
INSERT INTO `t_sys_role_resource` VALUES (7, 85);
INSERT INTO `t_sys_role_resource` VALUES (7, 86);
INSERT INTO `t_sys_role_resource` VALUES (7, 87);
INSERT INTO `t_sys_role_resource` VALUES (7, 88);
INSERT INTO `t_sys_role_resource` VALUES (7, 89);
INSERT INTO `t_sys_role_resource` VALUES (7, 93);
INSERT INTO `t_sys_role_resource` VALUES (7, 94);
INSERT INTO `t_sys_role_resource` VALUES (7, 95);
INSERT INTO `t_sys_role_resource` VALUES (7, 96);
INSERT INTO `t_sys_role_resource` VALUES (7, 97);
INSERT INTO `t_sys_role_resource` VALUES (7, 98);
INSERT INTO `t_sys_role_resource` VALUES (7, 99);
INSERT INTO `t_sys_role_resource` VALUES (7, 100);
INSERT INTO `t_sys_role_resource` VALUES (7, 101);
INSERT INTO `t_sys_role_resource` VALUES (7, 102);
INSERT INTO `t_sys_role_resource` VALUES (7, 103);
INSERT INTO `t_sys_role_resource` VALUES (7, 109);
INSERT INTO `t_sys_role_resource` VALUES (7, 110);
INSERT INTO `t_sys_role_resource` VALUES (7, 111);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_user`;
CREATE TABLE `t_sys_role_user` (
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-角色用户关系表';

-- ----------------------------
-- Records of t_sys_role_user
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_role_user` VALUES (4, 7);
INSERT INTO `t_sys_role_user` VALUES (5, 9);
INSERT INTO `t_sys_role_user` VALUES (8, 10);
INSERT INTO `t_sys_role_user` VALUES (18, 22);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称、真实姓名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(256) DEFAULT NULL COMMENT '密码',
  `salt` varchar(32) DEFAULT NULL COMMENT '盐值',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱地址',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号(预留位数)',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别: UNKNOW、MALE、FEMALE',
  `status` int(11) DEFAULT NULL COMMENT '当前状态（0 禁用，1可用）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_sys_user.username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='系统-用户表';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_user` VALUES (4, '小韩', 'han.q', '1faa2dee5d3d4d8152d34cbb449b4e4f', '3bcd1b3e7b21fd3feea34b237402be98', '', '15901726887', NULL, 1);
INSERT INTO `t_sys_user` VALUES (5, '小号韩庆', 'han.qing', 'f0c4736e700b935590f87bca3a4a7502', 'b3a9dc4f4c5d86aeb1fd74d0ba89f292', '', '', NULL, 1);
INSERT INTO `t_sys_user` VALUES (8, '只读用户', 'hanqing2', '08e8b70a6cd7942d73b5c07bee5472ef', '423a620927571b3da1c92d9b996fda0e', '583746309@qq.com', '15901726887', NULL, 1);
INSERT INTO `t_sys_user` VALUES (18, 'SSO用户', 'SSO_USER', 'a2253813154386c83c60960e5e7fb053', 'c02993cdccda06f6f8e397fcca8efec0', NULL, NULL, NULL, 1);
INSERT INTO `t_sys_user` VALUES (19, '独立团', 'DULI', '39f95bde52a416fe1bf6278c75e1bd23', '843c6131b41abf17ed62d01684ebd3e6', '', '', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_user_app_____del
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_app_____del`;
CREATE TABLE `t_sys_user_app_____del` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `app_id` int(11) NOT NULL COMMENT '应用ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_sys_user_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_resource`;
CREATE TABLE `t_sys_user_resource` (
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `resource_id` int(11) DEFAULT NULL COMMENT '资源ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统用户-资源关系表\n（适用于弱角色场景、减少角色处理场景）- 暂不用';

-- ----------------------------
-- Records of t_sys_user_resource
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_user_resource` VALUES (19, 77);
INSERT INTO `t_sys_user_resource` VALUES (19, 78);
INSERT INTO `t_sys_user_resource` VALUES (19, 79);
INSERT INTO `t_sys_user_resource` VALUES (19, 80);
INSERT INTO `t_sys_user_resource` VALUES (19, 91);
INSERT INTO `t_sys_user_resource` VALUES (19, 104);
INSERT INTO `t_sys_user_resource` VALUES (19, 105);
INSERT INTO `t_sys_user_resource` VALUES (19, 106);
INSERT INTO `t_sys_user_resource` VALUES (19, 107);
INSERT INTO `t_sys_user_resource` VALUES (19, 108);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
