package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.manager.IPermissionService;
import com.hoo.permission.sdk.server.service.ISysUserService;
import com.hoo.permission.sdk.web.config.SafeConfig;
import com.hoo.permission.sdk.web.security.jwt.HooJwtTools;
import com.hoo.permission.sdk.web.security.shiro.SecurityUtils;
import com.hoo.permission.sdk.server.domain.entity.SysUser;
import com.hoo.permission.sdk.server.domain.model.LoginUser;
import com.hoo.permission.sdk.server.domain.model.UserStatus;
import com.hoo.permission.sdk.server.domain.pojo.LoginPo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * （自身）权限相关控制器
 * 1、当前用户信息
 * 2、当前用户权限信息
 * @author hank
 * @create 2020-07-04 上午11:22
 **/
@Api(tags = "权限模块", description = "用户登录、登出、导航菜单等")
@RestController
@RequestMapping("auth")
public class PermissionController {

    @Autowired IPermissionService permissionService;
    @Autowired ISysUserService userService;

    @Autowired HooJwtTools jwtTools;

    @Autowired SafeConfig safeConfig;

    /**
     * 普通登录入口，支持用户名、密码以及其他验证场景
     * @param model
     * @return
     */
    @ApiOperation("登录")
    @PostMapping("login")
    public R login(LoginPo model) {
        // 正则： 手机号（可走验证码）、邮箱、普通用户名, 目前先支持 普通用户名
        SysUser user = userService.getByUserName(model.getUsername());
        if(user == null) {
            // 迷惑性提示
            return R.error("用户名或密码错误");
        }
        if (null == user.getStatus()) {
            return R.error("用户信息异常，请联系管理员");
        } else if (UserStatus.DISABLED == user.getStatus()) {
            return R.error("用户已禁用，请联系管理员");
        } else if (UserStatus.LOCKED == user.getStatus()) {
            return R.error("用户已被锁定，请稍后再尝试登录");
        }
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getSalt());
        SimpleHash simpleHash = new SimpleHash(safeConfig.getHashAlgorithmName(), model.getPassword(), credentialsSalt, safeConfig.getHashIterations());

        try {

            if(user.getPassword().equals(simpleHash.toString())) {
                // 成功登录, 返回用户基本信息、权限标记、菜单信息
                Map<String,Object> data = new HashMap<>(1);
                data.put("jwtToken", jwtTools.sign(new LoginUser(user.getId(), user.getUsername(), user.getPassword())));
                return R.data(data);
            } else {
                return R.error("用户名或密码错误");
            }
        }catch (Exception e) {
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    @ApiOperation("退出")
    @PostMapping("logout")
    public R logout() {
        // 考虑做黑名单, 在黑名单（即已经退出的用户）中，则验证token时，做非法处理
        SecurityUtils.logout();
        return R.ok();
    }

    @ApiOperation("导航菜单")
    @GetMapping("nav")
    public R nav() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(loginUser == null) {
            return R.error("无权操作");
        }
        return R.data(permissionService.queryResource(loginUser.getId()));
    }

}
