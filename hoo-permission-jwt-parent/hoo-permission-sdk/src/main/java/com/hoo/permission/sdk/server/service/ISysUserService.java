package com.hoo.permission.sdk.server.service;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.pojo.SysUserPo;
import com.hoo.permission.sdk.server.domain.entity.SysUser;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统-用户 服务接口
 */
public interface ISysUserService {
    /**
     * 查询-分页
     * @param page
     * @param params
     * @return
     */
    Page query(Page page, Map<String,Object> params);

    /**
     * 保存 用户信息
     * @param userDto
     * @return
     */
    SysUser save(SysUserPo userDto);

    /**
     * 修改用户信息(此方法 不修改 username、password、salt)
     * @param userDto
     * @return
     */
    SysUser update(SysUserPo userDto);

    /**
     * 状态变更
     * @param id
     * @param status
     * @return
     */
    boolean updateStatus(Long id, int status);

    /**
     * 删除用户
     * @param username
     * @return
     */
    boolean delete(String username);

    /**
     * 批量删除用户
     * @param ids
     * @return
     */
    boolean batchDelete(List<Long> ids);

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    SysUser getByUserName(String username);

    SysUser get(Serializable id);

    /**
     * 密码加权处理
     * @param credentials
     * @param salt
     * @return
     */
    String passwordEncoder(String credentials, String salt);

    /**
     * 密码修改
     * @param username
     * @param oldPassword
     * @param newPassword
     */
    void changePassword(String username, String oldPassword, String newPassword);

    /**
     * 重置密码
     * @param username
     * @param newPassword
     */
    void resetPassword(String username, String newPassword);
}
