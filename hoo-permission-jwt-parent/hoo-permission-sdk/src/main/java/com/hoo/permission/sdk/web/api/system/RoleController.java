package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.Page;
import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.dao.ISysRoleDao;
import com.hoo.permission.sdk.server.domain.entity.SysRole;
import com.hoo.permission.sdk.server.domain.pojo.SysRolePo;
import com.hoo.permission.sdk.server.service.ISysRoleService;
import com.hoo.permission.sdk.server.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 角色控制器
 *
 * 1、角色分页查询（角色列表）
 * 2、全部角色查询（用于角色分配时下拉场景）
 * 3、添加角色、修改角色、删除角色（角色自身信息 + 关联资源信息）
 *
 * @author hank
 * @create 2020-06-28 下午7:21
 **/
@Api(tags = "角色管理", description = "用户CRUD等功能")
@RestController
@RequestMapping("role")
public class RoleController {

    @Autowired ISysRoleService roleService;

    @Autowired ISysRoleDao roleDao;

    /**
     * 分页查询 角色列表
     * @return
     */
    @ApiOperation("角色信息")
    @GetMapping
    @RequiresPermissions("sys:role:list")
    public R query(Integer pageNo, Integer pageSize, String searchWord, String status) {
        Map<String,Object> params = new HashMap<>();
        params.put("searchWord", searchWord);
        params.put("status", status);
        return R.page(roleDao.query(new Page(pageNo, pageSize), params));
    }

    /**
     * 查询全部 角色
     * @return
     */
    @ApiOperation("全部角色信息")
    @GetMapping("/all")
    public R queryAll() {
        return R.data(roleService.queryAll(new HashMap<>()));
    }

    @ApiOperation("角色信息转字典")
    @GetMapping("/dictionary")
    public R dictionary(Map<String,Object> params) {
        List<SysRole> list = roleService.queryAll(params);
        List<Map<String,Object>> res = new ArrayList<>();
        list.stream().forEach(record -> {
            Map<String,Object> item = new HashMap<>();
            item.put("value", record.getId());
            item.put("label", record.getName());
            res.add(item);
        });
        return R.data(res);
    }

    /**
     * 添加角色（包含分配资源权限）
     * @param roleDto
     * @return
     */
    @ApiOperation("新增角色")
    @PostMapping
    @RequiresPermissions("sys:role:save")
    public R save(SysRolePo roleDto) {
        try {
            roleService.save(roleDto);
        }catch (DuplicateKeyException e){
            return R.error(roleDto.getRoleKey() + "已存在，不可重复添加");
        }
        return R.ok();
    }

    @ApiOperation("修改角色")
    @PutMapping
    @RequiresPermissions("sys:role:update")
    public R update(SysRolePo roleDto) {
        roleService.save(roleDto);
        return R.ok();
    }

    @ApiOperation("修改角色状态")
    @PutMapping("/updateStatus")
    @RequiresPermissions({"sys:role:disabled", "sys:role:useable"})
    public R updateStatus(Long id, String status) {
        if (id == null || id < 0) {
            return R.error("参数不合法");
        }
        return R.data(roleService.updateStatus(id, status));
    }

    /**
     * 删除角色
     * @return
     */
    @ApiOperation("删除角色")
    @DeleteMapping("/{ids}")
    @RequiresPermissions("sys:role:delete")
    public R del(@PathVariable("ids") String ids) {
        roleService.delete(StringUtil.parseLong(ids));
        return R.ok();
    }

}
