package com.hoo.permission.sdk.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * sso相关配置
 *
 * @author hank
 * @create 2020-08-09 下午4:51
 **/
@Configuration
@ConfigurationProperties(prefix = "hoo.permission.sso")
public class SsoConfig {
    /**
     * app key
     */
    private String appKey;
    /**
     * app secret
     */
    private String appSecret;


    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
