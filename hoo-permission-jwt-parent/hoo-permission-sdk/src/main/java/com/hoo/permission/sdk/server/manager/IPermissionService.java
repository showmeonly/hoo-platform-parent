package com.hoo.permission.sdk.server.manager;

import com.alibaba.fastjson.JSONArray;
import com.hoo.permission.sdk.server.domain.entity.SysResource;
import com.hoo.permission.sdk.server.domain.entity.SysRole;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 权限服务
 * @author hank
 */
public interface IPermissionService {
    /**
     * 获取用于权限集合
     * @param userId
     * @return
     */
    Set<String> getPermissions(Long userId);

    /**
     * 获取用户菜单信息
     * @param userId
     * @return
     */
    JSONArray getMenus(Long userId);

    /**
     * 获取用户菜单信息
     * @param userId
     * @return
     */
    List<SysResource> queryResource(Long userId);

    /**
     * 获取用户角色信息
     * @param userId
     * @return
     */
    List<SysRole> getRoles(Serializable userId);
}
