package com.hoo.permission.sdk.web.security.shiro;

import com.hoo.common.model.R;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * shrio相关异常全局捕捉
 *
 * @author hank
 * @create 2020-11-12 下午2:09
 **/
@ControllerAdvice
public class ShiroExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(value = {AuthorizationException.class})
    public R authorizationException(AuthorizationException e) {
        e.printStackTrace();
        return R.error("无权访问");
    }

    @ResponseBody
    @ExceptionHandler(value = {SecurityException.class})
    public R securityException(SecurityException e) {
        e.printStackTrace();
        return R.error(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {AuthenticationException.class})
    public R authenticationException(AuthenticationException e) {
        e.printStackTrace();
        return R.error(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {Exception.class})
    public R exception(Exception e) {
        e.printStackTrace();
        return R.error(e.getMessage());
    }
}
