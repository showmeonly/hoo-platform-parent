package com.hoo.permission.sdk.server.dao.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysAppDao;
import com.hoo.permission.sdk.server.domain.entity.SysApp;
import com.hoo.permission.sdk.server.domain.pojo.SysAppPo;
import com.hoo.permission.sdk.server.util.AppUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 系统接入应用表，用于支持SSO多平台权限控制 Dao层 实现
 * @author 小韩工作室
 * @date 2020-08-09 17:19:00
 */
@Repository
public class SysAppDaoImpl implements ISysAppDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean add(SysApp entity){
        String appKey = AppUtil.getAppKey();
        String appSecret = AppUtil.getAppSecret("" + System.nanoTime(), appKey);
        entity.setAppKey(appKey);
        entity.setSecret(appSecret);
        return jdbcTemplate.update("INSERT INTO t_sys_app(id, name, description, secret, frontend, sort, app_key) VALUES(?, ?, ?, ?, ?, ?, ?)", entity.getId(), entity.getName(), entity.getDescription(), entity.getSecret(), entity.getFrontend(), entity.getSort(), entity.getAppKey()) > 0;
    }

    @Override
    public boolean update(SysApp entity) {
        // appkey 不更新
        return jdbcTemplate.update("UPDATE t_sys_app SET id = ? ,name = ? ,description = ? ,secret = ? ,frontend = ? ,sort = ?  WHERE id = ?", entity.getId(), entity.getName(), entity.getDescription(), entity.getSecret(), entity.getFrontend(), entity.getSort(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(SysApp entity){
        return jdbcTemplate.update("DELETE FROM t_sys_app WHERE id = ?", entity.getId()) > 0;
    }

    @Override
    public SysApp get(Serializable id) {
        List<SysApp> list = jdbcTemplate.query("SELECT id, name, description, secret, frontend, sort, app_key as appKey FROM t_sys_app WHERE id = ? ", new Object[]{ id }, new BeanPropertyRowMapper<SysApp>(SysApp.class));
        if(list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SysApp> findAll(Map< String, Object> params) {
        List<SysApp> list = jdbcTemplate.query("SELECT id, name, description, secret, frontend, sort, app_key as appKey FROM t_sys_app WHERE 1=1 ", new Object[]{}, new BeanPropertyRowMapper<SysApp>(SysApp.class));
        if(list != null && list.size() > 0) {
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public boolean batchDelete(List<Long> ids) {
        String sql="DELETE FROM t_sys_app WHERE id = ?";
        List<Object[]> batchArgs = new ArrayList<Object[]>();
        for(Long id : ids) {
            batchArgs.add(new Object[]{id});
        }
        return jdbcTemplate.batchUpdate(sql, batchArgs).length == ids.size();
    }

    @Override
    public Page<SysApp> query(Page page, SysAppPo params) {
        if (page == null) {
          page = new Page();
        }
        List<Object> args = new ArrayList<Object>();
        StringBuilder where = new StringBuilder();
        // 查询参数 处理
        if(!StringUtils.isEmpty(params.getName())) {
            where.append(" AND name LIKE ?");
            args.add("%" + params.getName() + "%");
        }
        if(!StringUtils.isEmpty(params.getFrontend())) {
            where.append(" AND frontend = ?");
            args.add(params.getFrontend());
        }
        Long total = jdbcTemplate.queryForObject(new StringBuffer("SELECT COUNT(1) FROM t_sys_app WHERE 1=1 ").append(where).toString(), Long.class, args.toArray());
        if (total != null && total > 0) {
            args.add((page.getPageNo() - 1) * page.getLimit());
            args.add(page.getLimit());
            List<SysApp> list = jdbcTemplate.query(new StringBuffer("SELECT id, name, description, secret, frontend, sort, app_key as appKey FROM t_sys_app WHERE 1=1 ")
                            .append(where).append(" ORDER BY sort ASC").append(" LIMIT ?,?").toString(),
                    args.toArray(), new BeanPropertyRowMapper<>(SysApp.class));
            if(list != null && list.size() > 0) {
                page.setRecords(list);
            }
        }
        page.setTotal(total);
        return page;
    }

}
