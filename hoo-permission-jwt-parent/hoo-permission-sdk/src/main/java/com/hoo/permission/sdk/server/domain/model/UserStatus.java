package com.hoo.permission.sdk.server.domain.model;

/**
 * 用户状态
 * com.hoo.permission.sdk.server.domain.entity.SysUser.status
 * @author hank
 * @create 2020-07-01 下午10:49
 **/

public interface UserStatus {
    /**
     * 不可用
     */
    int DISABLED = 0;
    /**
     * 可用
     */
    int VALID = 1;
    /**
     * 锁定
     */
    int LOCKED = 2;
}
