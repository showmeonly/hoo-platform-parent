package com.hoo.permission.sdk.server.domain.pojo;

import com.hoo.permission.sdk.server.domain.entity.SysRole;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色PO层
 *
 * @author hank
 * @create 2020-06-28 下午7:42
 **/

public class SysRolePo extends SysRole {

    /*private String resourceIdStr;
    private Long[] resourceIds;

    public String getResourceIdStr() {
        return resourceIdStr;
    }

    public void setResourceIdStr(String resourceIdStr) {
        this.resourceIdStr = resourceIdStr;
    }

    public Long[] getResourceIds() {
        if(!StringUtils.isEmpty(resourceIdStr) && resourceIds == null) {
            List<Long> resourceIds = new ArrayList<>();
            for (String item : resourceIdStr.split(",")) {
                resourceIds.add(Long.valueOf(item));
            }
            return resourceIds.toArray(new Long[]{});
        }
        return resourceIds;
    }

    public void setResourceIds(Long[] resourceIds) {
        this.resourceIds = resourceIds;
    }*/

    private List<Long> resourceIds;

    public List<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

}
