package com.hoo.permission.sdk.server.domain.model;

/**
 * 操作类型
 * @author hank
 */
public enum OperationType {
    ADD("add", "新增操作"),
    UPDATE("update", "修改操作"),
    DELETE("delete", "删除操作");

    OperationType(String value, String description){
        this.value = value;
        this.description = description;
    }

    private String description;
    private String value;

    @Override
    public String toString() {
        return value;
    }
}
