package com.hoo.permission.sdk.server.domain.entity;

import java.io.Serializable;

/**
 * 系统-角色资源关系表
 * @author 小韩工作室
 * @date 2020-06-28 18:15:48
 */
public class SysRoleResource implements Serializable {
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 资源ID
     */
    private Long resourceId;

    public Long getRoleId(){
        return this.roleId;
    }

    public Long getResourceId(){
        return this.resourceId;
    }

    public void setRoleId(Long roleId){
        this.roleId = roleId;
    }

    public void setResourceId(Long resourceId){
        this.resourceId = resourceId;
    }

}
