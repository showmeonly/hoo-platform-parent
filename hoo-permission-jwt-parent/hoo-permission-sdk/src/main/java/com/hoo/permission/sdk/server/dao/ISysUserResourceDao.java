package com.hoo.permission.sdk.server.dao;

import com.hoo.permission.sdk.server.domain.entity.SysUserResource;

import java.io.Serializable;
import java.util.List;

/**
 * 系统用户-资源关系表（适用于弱角色场景、减少角色处理场景) Dao层接口
 * @author 小韩工作室
 * @date 2020-08-09 17:19:00
 */
public interface ISysUserResourceDao {
    /**
     * 新增 - 系统用户-资源关系表（适用于弱角色场景、减少角色处理场景)
     */
    // boolean add(SysUserResource entity);

    /**
     * 删除 - 系统用户-资源关系表（适用于弱角色场景、减少角色处理场景)
     */
    // boolean delete(SysUserResource entity);


    /**
     * 新增 - 系统-用户资源关系表
     * @param userId
     * @param resourceIds
     * @return
     */
    boolean add(Long userId, List<Long> resourceIds);

    /**
     * 通过 userId 删除 - 系统-用户资源关系
     * @param userId
     * @return
     */
    boolean deleteByUserId(Long userId);

    /**
     * 通过 resourceId 删除 - 系统-用户资源关系
     * @param resourceId
     * @return
     */
    boolean deleteByResourceId(Long resourceId);

    /**
     * 通过 resourceIds 批量删除 - 系统-用户资源关系
     * @param resourceIds
     * @return
     */
    boolean batchDeleteByResourceIds(List<Long> resourceIds);

    /**
     * 根据用户ID 获取 独立资源信息
     * @param userId
     * @return
     */
    List<Long> getResourcesByUserId(Serializable userId);
}
