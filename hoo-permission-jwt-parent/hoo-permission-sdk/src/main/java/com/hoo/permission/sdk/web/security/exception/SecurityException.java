package com.hoo.permission.sdk.web.security.exception;


/**
 * 安全相关异常
 *
 * @author hank
 * @create 2020-11-12 下午8:21
 **/

public class SecurityException extends Exception {
}
