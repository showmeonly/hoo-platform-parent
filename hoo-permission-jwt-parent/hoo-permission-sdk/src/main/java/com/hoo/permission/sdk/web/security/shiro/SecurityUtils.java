package com.hoo.permission.sdk.web.security.shiro;

import com.hoo.permission.sdk.web.security.jwt.HooJwtTools;
import com.hoo.permission.sdk.server.domain.model.LoginUser;
import com.hoo.permission.sdk.server.util.SpringContextUtil;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * SecurityUtils
 *
 * @author hank
 * @create 2020-11-12 下午4:03
 **/

public class SecurityUtils {

    /**
     * 获取当前登录用户
     * @return
     */
    public static LoginUser getLoginUser() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();

        HooJwtTools jwtTools = SpringContextUtil.getBean(HooJwtTools.class);
        return jwtTools.getLoginUser(request);
    }

    /**
     * 登出
     */
    public static void logout() {

    }

}
