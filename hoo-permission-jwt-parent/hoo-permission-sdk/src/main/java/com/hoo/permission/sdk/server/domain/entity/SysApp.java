package com.hoo.permission.sdk.server.domain.entity;

import java.io.Serializable;

/**
 * 系统接入应用表，用于支持SSO多平台权限控制
 * @author 小韩工作室
 * @date 2020-08-09 17:14:43
 */
public class SysApp implements Serializable {
    /**
     * ID主键
     */
    private Integer id;
    /**
     * 系统接入名称(applicaiton.name)
     */
    private String name;
    /**
     * 系统描述
     */
    private String description;
    /**
     * app key （唯一,分配不可更改）
     */
    private String appKey;
    /**
     * 系统接入密钥
     */
    private String secret;
    /**
     * 前端类型（VUE、ExtJS、CUSTOM、React、Angular）
     * 后续可用于返回菜单时，默认转换处理为指定类型标准的格式（不指定则默认树形不处理，由实际应用来转换实现）
     */
    private String frontend;
    /**
     * 排序序号
     */
    private Integer sort;

    public Integer getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public String getDescription(){
        return this.description;
    }

    public String getSecret(){
        return this.secret;
    }

    public String getFrontend(){
        return this.frontend;
    }

    public Integer getSort(){
        return this.sort;
    }

    public String getAppKey(){
        return this.appKey;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setSecret(String secret){
        this.secret = secret;
    }

    public void setFrontend(String frontend){
        this.frontend = frontend;
    }

    public void setSort(Integer sort){
        this.sort = sort;
    }

    public void setAppKey(String appKey){
        this.appKey = appKey;
    }

}
