package com.hoo.permission.sdk.web.security.jwt;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * JWTToken
 *
 * @author hank
 * @create 2020-11-12 下午5:32
 **/

public class HooJWTToken implements AuthenticationToken {

    private String token;

    public HooJWTToken(){}

    public HooJWTToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
