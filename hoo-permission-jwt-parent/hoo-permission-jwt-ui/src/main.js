// with polyfills
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
import { VueAxios } from './utils/request'

// mock
// WARNING: `mockjs` NOT SUPPORT `IE` PLEASE DO NOT USE IN `production` ENV.
import './mock'

import bootstrap from './core/bootstrap'
import './core/lazy_use'
import './permission' // permission control
import './utils/filter' // global filter
import './components/global.less'
import dic from './utils/dic'

Vue.config.productionTip = false

// 注入字典信息(开发阶段使用)
dic.put('sys_role_state', [{ label: '正常', value: '1' }, { label: '禁用', value: '0' }])
dic.put('sys_user_state', [{ label: '正常', value: 1 }, { label: '禁用', value: 0 }, { label: '锁定', value: 2 }])
dic.put('sys_res_state', [{ label: '可用', value: '1' }, { label: '禁用', value: '0' }])
dic.put('sys_res_type', [{ label: '菜单', value: 1 }, { label: '按钮', value: 2 }])
// mount axios Vue.$http and this.$http
Vue.use(VueAxios)
Vue.prototype.$dic = dic

new Vue({
  router,
  store,
  created: bootstrap,
  render: h => h(App)
}).$mount('#app')
