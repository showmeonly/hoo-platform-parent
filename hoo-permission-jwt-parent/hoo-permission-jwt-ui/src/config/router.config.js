// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
// import { bxAnaalyse } from '@/core/icons'

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/dashboard/workplace',
    children: [{
      path: '/dashboard/workplace',
      name: 'WorkPlace',
      component: () => import('@/views/dashboard/Workplace'),
      meta: { title: '工作台2', keepAlive: false, icon: 'dashboard' }
    }, {
      path: '/permission',
      name: 'permission',
      redirect: '/permission/user',
      component: RouteView,
      meta: { title: '权限管理', keepAlive: true, icon: 'control' },
      children: [
        {
          path: '/permission/user',
          name: 'User',
          component: () => import('@/views/permission/UserList'),
          meta: { title: '用户管理', keepAlive: false }
        },
        {
          path: '/permission/role',
          name: 'Role',
          component: () => import('@/views/permission/RoleList'),
          meta: { title: '角色管理', keepAlive: false }
        },
        {
          path: '/permission/resource',
          name: 'Resource',
          component: () => import('@/views/permission/ResourceList'),
          meta: { title: '资源管理', keepAlive: false }
        }
      ]
    }]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import('@/views/user/RegisterResult')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/exception/404')
  }
]
