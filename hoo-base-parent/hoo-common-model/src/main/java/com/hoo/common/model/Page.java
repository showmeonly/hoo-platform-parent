package com.hoo.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 分页模型
 *
 * @author hank
 * @create 2020-06-29 下午3:08
 **/

public class Page<T> implements Serializable {
    private Integer pageNo;
    private Integer limit;
    /**
     * 兼容 分页参数 同 limit, 若合法则使用之
     */
    private Integer pageSize;
    private List<T> records = new ArrayList<T>();
    private long total;

    public Page(){}

    public Page(int pageNo, int limit) {
        this.pageNo = pageNo;
        this.limit = limit;
    }

    public Integer getPageNo() {
        if(pageNo == null || pageNo < 1) {
            pageNo = 1;
        }
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getLimit() {
        if(limit == null || limit < 1) {
            limit = 10;
        }
        return (pageSize != null && pageSize > 0) ? pageSize : limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
