package com.example.hoo.permission.jwt.sdk;

import com.hoo.common.json.JSONUtils;
import com.hoo.permission.sdk.server.domain.pojo.SysRolePo;
import com.hoo.permission.sdk.server.domain.pojo.SysUserPo;
import com.hoo.permission.sdk.server.domain.entity.SysUser;
import com.hoo.permission.sdk.server.manager.IPermissionService;
import com.hoo.permission.sdk.server.service.ISysResourceService;
import com.hoo.permission.sdk.server.service.ISysRoleService;
import com.hoo.permission.sdk.server.service.ISysUserService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class HooPermissionJwtSdkApplicationTests {

    @Autowired
    ISysUserService userService;
    @Autowired
    ISysRoleService roleService;
    @Autowired
    ISysResourceService resourceService;
    @Autowired
    IPermissionService permissionService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    void contextLoads() {


    }

    // @Test
    void user() {
        // 1、新增
        SysUserPo userDto = new SysUserPo();
        userDto.setUsername("han.q");
        userDto.setNickname("韩庆");
        userDto.setPassword("123456");
        SysUser user = userService.save(userDto);

        logger.info("user:{}", JSONUtils.toJSONString(user));

        // 2、查询
        logger.info("userService.getByUserName 结果:" + JSONUtils.toJSONString(userService.getByUserName(user.getUsername())));

        // 3、修改
        userDto.setNickname("小韩");
        userDto.setMobile("15901726887");
        logger.info("{}", userService.update(userDto));

        // 4、删除
        // userService.delete("han.q");
    }

    // @Test
    void role() {
        SysRolePo roleDto = new SysRolePo();
        roleDto.setName("超级管理员");
        roleDto.setDescription("系统最高用户,拥有所有权限");

        // 1、新增
        roleService.save(roleDto);
        // 查询
        logger.info("新增后查询:{}", JSONUtils.toJSONString(roleService.queryAll(new HashMap<>())));

        // 2、修改
        roleDto.setName("超级管理员-update");
        roleService.save(roleDto);
        // 查询
        logger.info("修改后查询:{}", JSONUtils.toJSONString(roleService.queryAll(new HashMap<>())));

        // 3、删除
        // roleService.delete(Arrays.asList(new Long[]{roleDto.getId()}));
        // 查询
        // logger.info("删除后查询:{}", JSONUtils.toJSONString(roleService.queryAll(new HashMap<>())));
    }

    // @Test
    void resource() {

        /*for(int i = 0,len = 3; i < len; i++){
            // 加 菜单
            SysResource parent = new SysResource();
            parent.setName("系统菜单-" + i);
            parent.setType(1);
            resourceService.add(parent);
            // 套菜单 / 按钮
            SysResource child = null;
            for(int j = 0, lenJ = 10; j < lenJ; j++) {
                if(j > 0 && j % 3 == 0) {
                    // 按钮
                    SysResource button = new SysResource();
                    button.setType(2);
                    button.setParentId(child.getId());
                    button.setName("子菜单-按钮" + i);
                    resourceService.add(button);
                } else {
                    // 菜单
                    child = new SysResource();
                    child.setParentId(parent.getId());
                    child.setName("系统（二级）子菜单-" + i);
                    child.setType(1);
                    resourceService.add(child);
                }
            }
        }*/

        Map<String, Object> params = new HashMap<>();
        // params.put("type", 1);
        logger.info("resourceService.queryList: {}", JSONUtils.toJSONString(resourceService.queryList(params)));
        logger.info("resourceService.queryTree: {}", JSONUtils.toJSONString(resourceService.queryTree(params)));

    }

    // @Test
    void perm() {
        logger.info("permissionService.getPermissions: {}", permissionService.getPermissions(4L));
        logger.info("permissionService.getMenus:{}", permissionService.getMenus(4L));
    }

    // @Test
    void testSort() {
        List<Long> curResIds = new ArrayList<>();
        List<Long> resourceIds = new ArrayList<>();
        curResIds.add(1L);
        curResIds.add(2L);
        curResIds.add(0L);
        resourceIds.add(0L);
        resourceIds.add(1L);
        resourceIds.add(2L);

        Comparator sort = new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                return o1.compareTo(o2);
            }
        };
        curResIds.sort(sort);
        resourceIds.sort(sort);

        logger.info("equals:{}", curResIds.equals(resourceIds));
    }
}
